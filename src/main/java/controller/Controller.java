package controller;

import first.task.FirstTask;
import fourth.task.FourthTask;
import second.task.SecondTask;
import third.task.ThirdTask;

public class Controller {

  FirstTask firstTask;
  SecondTask secondTask;
  ThirdTask thirdTask;
  FourthTask fourthTask;

  public Controller() {
    firstTask = new FirstTask();
    secondTask = new SecondTask();
    thirdTask = new ThirdTask();
    fourthTask = new FourthTask();
  }

  public void firsTaskMax() {
    firstTask.firstTaskMax();
  }

  public void firstTaskAverage() {
    firstTask.firstTaskAverage();
  }

  public void secondTaskTest() {
    secondTask.secondTask();
  }

  public void thirdTaskTest() {
    thirdTask.thirdTaskTest();
  }

  public void fourthTaskTest() {
    fourthTask.fourthTaskTest();
  }
}
