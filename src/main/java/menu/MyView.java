package menu;

import controller.Controller;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  public MyView() {
    controller = new Controller();
    menu = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    menu.put("1", "1 - First task max");
    menu.put("2", "2 - First task average");
    menu.put("3", "3 - Second task test");
    menu.put("4", "4 - Third task test");
    menu.put("5", "5 - Fourth task test");
    menu.put("Q", "Quit");

    methodsMenu.put("1", this::firstTaskMax);
    methodsMenu.put("2", this::firstTaskAverage);
    methodsMenu.put("3", this::secondTaskTest);
    methodsMenu.put("4", this::thirdTaskTest);
    methodsMenu.put("5", this::fourthTaskTest);
  }

  private void outputMenu() {
    System.out.println("\n Menu: ");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  private void firstTaskMax() {
    controller.firsTaskMax();
  }

  private void firstTaskAverage() {
    controller.firstTaskAverage();
  }

  private void secondTaskTest() {
    controller.secondTaskTest();
  }

  private void thirdTaskTest() {
    controller.thirdTaskTest();
  }

  private void fourthTaskTest() {
    controller.fourthTaskTest();
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please select menu point: ");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();

      } catch (Exception e) {

      }
    } while (!keyMenu.equals("Q"));
  }


}
