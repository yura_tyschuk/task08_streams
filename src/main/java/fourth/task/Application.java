package fourth.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {

  private List<String> allWords;
  private String stringToAdd;
  private Scanner scanner = new Scanner(System.in);

  Application() {
    allWords = new ArrayList<>();
    while (true) {
      System.out.println("Enter word(' '- to Quit) : ");
      stringToAdd = scanner.nextLine();
      if (stringToAdd.isEmpty()) {
        break;
      }

      allWords.add(stringToAdd);
    }
  }

  public void printList() {
    System.out.println(allWords);
  }

  public int getNumberOfUniqueWords() {
    return calculateNumberOfUniqueWords();
  }

  private int calculateNumberOfUniqueWords() {
    return createListOfUniqueWords().size();
  }

  private List<String> createListOfUniqueWords() {
    List<String> uniqueWordsList = allWords.stream()
        .map(i -> i)
        .distinct()
        .collect(Collectors.toList());
    return uniqueWordsList;
  }

  public List<String> sortListOfUniqueWords() {
    List<String> sortedUniqueWordsList = createListOfUniqueWords()
        .stream()
        .sorted()
        .collect(Collectors.toList());
    return sortedUniqueWordsList;
  }


  public void calculateNumberOfEachWords() {
    calculateNumberOfSymbols(allWords);
  }

  public void splitWordsOnSymbols() {
    List<String> symbolList = allWords.stream()
        .flatMap(e -> Stream.of(e.split("")))
        .filter(str -> Character.isLowerCase(str.charAt(0)))
        .collect(Collectors.toList());

    calculateNumberOfSymbols(symbolList);

  }

  private void calculateNumberOfSymbols(List<String> symbolList) {
    Map<String, Long> counts =
        symbolList.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));

    for (Map.Entry<String, Long> entry : counts.entrySet()) {
      System.out.println(entry.getKey() + ":" + entry.getValue().toString());
    }
  }
}

