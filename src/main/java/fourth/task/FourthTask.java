package fourth.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FourthTask {

  private static Logger logger = LogManager.getLogger();

  public void fourthTaskTest() {
    Application application = new Application();
    application.printList();
    logger.fatal(application.getNumberOfUniqueWords());

    logger.fatal(application.sortListOfUniqueWords());
    application.calculateNumberOfEachWords();

    application.splitWordsOnSymbols();

  }
}
