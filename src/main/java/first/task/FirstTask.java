package first.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FirstTask {

  private static Logger logger = LogManager.getLogger();

  public void firstTaskMax() {
    FunctionalInterfaceForFirstTask ff = (x, y, z) -> (x > y) ? (x > z ? x : z)
        : (y > z ? y : z);
    logger.fatal("Max :" + ff.function(3, 6, 2));
  }

  public void firstTaskAverage() {
    FunctionalInterfaceForFirstTask ff1 = (x, y, z) -> ((x + y + z) / 2);
    System.out.println("Average: " + ff1.function(1, 2, 3));
  }
}
