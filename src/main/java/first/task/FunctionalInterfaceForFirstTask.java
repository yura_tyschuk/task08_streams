package first.task;

@FunctionalInterface
interface FunctionalInterfaceForFirstTask {

  int function(int x, int y, int z);
}
