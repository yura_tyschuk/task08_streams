package third.task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StreamsMethods {

  private static Logger logger = LogManager.getLogger();
  private List<Integer> listOfStatistic;
  private Stream<Integer> stream;

  public StreamsMethods(Stream<Integer> stream) {
    this.stream = stream;
    listOfStatistic = new ArrayList<>();
  }

  public List<Integer> countAvMinMaxSum() {
    Integer var = stream.max(Integer::compare).get();
    Integer minValue = stream.min(Integer::compare).get();
    Double average = stream
        .mapToInt(listOfIntegers -> listOfIntegers)
        .average()
        .orElse(0.0);

    Integer sumByReduce = stream.reduce(0, Integer::sum);
    Integer sumByMethod = stream.mapToInt(Integer::intValue).sum();
    logger.fatal("Max: " + var);
    logger.fatal("Min " + minValue);
    logger.fatal("Average: " + average);
    logger.fatal("Sum by reduce: " + sumByReduce);
    logger.fatal("Sum by method: " + sumByMethod);

    return listOfStatistic;
  }


  public void sumOfElementsBiggerThanAverage() {
    Double average = stream
        .mapToInt(listOfIntegers -> listOfIntegers)
        .average()
        .orElse(0.0);

    Integer sum = stream
        .filter(i -> i > average)
        .mapToInt(i -> i)
        .sum();

    logger.fatal("Sum of elements bigger than average: " + sum);
  }
}
