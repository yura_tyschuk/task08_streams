package third.task;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class ThirdTask {

  public void thirdTaskTest() {

    List<Integer> listOfIntegers = Arrays.asList(1, 2, 3, 4, 5, 6);
    Stream<Integer> streamList = listOfIntegers.stream();
    Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6);
    Stream<Integer> stream1 = Stream.of(new Integer[]{1, 2, 3, 4, 5, 6});
    Stream<Integer> stream2 = Stream.iterate(1, n -> n + 1).limit(7);
    Stream<Integer> stream3 = Stream.generate(new Random()::nextInt).limit(7);

    StreamsMethods streamsMethods = new StreamsMethods(streamList);
    streamsMethods.countAvMinMaxSum();
    streamsMethods.sumOfElementsBiggerThanAverage();

  }
}
