package second.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AnonymyousClass {

  private static Logger logger = LogManager.getLogger();

  public static void printStr(String str) {
    logger.fatal(str);
  }

}
