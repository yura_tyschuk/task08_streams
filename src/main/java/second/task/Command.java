package second.task;

@FunctionalInterface
public interface Command {

  public void function(String str);
}
