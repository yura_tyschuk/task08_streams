package second.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public interface CommandDefault {

  Logger logger = LogManager.getLogger();

  default void function(String str) {
    logger.fatal("Command object");
  }
}
