package second.task;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SecondTask {

  private static Logger logger = LogManager.getLogger();

  public void secondTask() {
    Command ff = x -> logger.fatal(x);
    ff.function("Lambda function");

    SecondTask secondTask = new SecondTask();
    secondTask.strPrintProgram(AnonymyousClass::printStr, "Method reference");

    List<Command> availableCommand = new ArrayList<>();
    availableCommand.add(new Command() {
      @Override
      public void function(String str) {
        logger.fatal("Anonymyous class");
      }
    });

  }

  public void strPrintProgram(Command tmp, String str) {
    tmp.function(str);
  }
}
